---
layout: default
title: Getting Started
has_children: false
nav_order: 2
---


# Getting Started
{: .fs-9 }

Incorporating open science practices into your research project. 
{: .fs-6 .fw-300 }

---
![open-lifecycle]({% link img/img-open-life-cycle.png %})

## The life cycle of a research project

At almost every stage of your research project, there are open science related practices you can incorporate. Here is an overview for the topics that are relevant in every stage, and links to the corresponding community pages.

## Planning & designing your study

- Funding requirements and writing your grant applications 
    - [Open data ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#governance)
    - [Data management plans ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/data-management-plans/)
- Ethics , DPIA 
    - [Open data > Can I share my data > Ethics ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#ethics)
    - [Open data > Governance ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/can-i/#governance)
- [Pre-registration ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/preregistration/)

## Collecting & managing data
- [MR protocols ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/protocols/)
- [Tasks/Paradigms ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/tasks/)
- [Data management ](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/data/data-management-plans/)
    
## Analysing & collaborating
- [Sharing analysis scripts](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/analysis/)
- [Git tutorials](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/gitlab-tutorials/)
    
## Disseminating & sharing your research
- [Publishing and licensing](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/publishing-licencing/)
- [Registered reports](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/preregistration/)

## Remaining Questions? 
- [How to get help?](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/)
- open@win.ox.ac.uk
