---
layout: default
title: Meet the Ambassadors
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 1
---

# Meet the Ambassadors
{: .fs-9 }

Find out about the current Open WIN Ambassadors
{: .fs-6 .fw-300 }

---

## What does it mean to be an Open WIN Ambassador?

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/1025477303?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture; clipboard-write" style="position:absolute;top:0;left:0;width:100%;height:100%;" title="What does it mean to be an Open WIN Ambassador?, October 2024"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

## The 2025 Ambassadors

![Open WIN Ambassadors 2025]({% link img/img-ambassadors-2025.png %})
{: .fs-3 .fw-300 }

### Tara Ghafari
Tara is a postdoc in neurosc lab at OHBA. She is in the process of relocating to Oxford, and her updated profile will be available in March. To see her profile at the University of Birmingham, click [here](https://www.birmingham.ac.uk/university/colleges/les/ecdn/reps/tara-ghafari).

*"I believe that reproducibility strengthens scientific findings and accelerates progress. Inclusivity is equally vital, it ensures that diverse voices and perspectives are welcomed, creating a more creative and supportive environment for everyone to thrive."*

### Kirralise Hansford
Kirralise is a postdoc in the Pain in Women and the EndoCaRe research group. More about Kirralise on her [profile page](https://www.wrh.ox.ac.uk/team/kirralise-hansford).

*"Open science practices are important to implement no matter what type of researcher you are / what your field of study is, however, I feel they are especially important when your work has a clinical focus or involves patient groups as participants. This is because research aimed at supporting people living with clinical conditions needs to be reproducible, replicable, and open for researchers, clinicians, and patients to access. Open science practices help ensure that research has the best chance of creating a lasting impact for the people who need it. I am excited to continue developing my open research practices as a WIN ambassador, and share knowledge of these practices with others within the WIN community."*

### Séamus O'Sullivan
Séamus is a WIN Experiment Support Officer and MEG lab manager at OHBA. Learn more on his [profile page](https://www.win.ox.ac.uk/people/seamus-osullivan).

*"I strongly believe in open science, and I want to help improve transparency and reproducibility in research.
As an experiment support officer, I work with researchers throughout WIN to help them set up and manage their experiments.
I am looking forward to helping researchers incorporate open science practices into their workflow, and building tools to facilitate these practices."*

### Yifan (Vivian) Yang
Yifan (Vivian) is a DPhil Student in the Department of Experimental Psychology. More about her on her [profile page](https://www.psy.ox.ac.uk/people/yifan-yang). 

*"Open science represents one of the best practices for enhancing research impact and quality while fostering a thriving academic community through transparent sharing of all study stages. Embedding open science principles into my doctoral studies from the outset will allow me to integrate cutting-edge practices into my work, advocate for them, and contribute meaningfully as a researcher. I very much look forward to learning from and collaborating with Open WIN."*

---

## The 2024 Ambassadors

![Open WIN Ambassadors 2024]({% link img/img-ambassadors-2024.png %})
{: .fs-3 .fw-300 }

### Anna Guttesen
Anna is a postdoc in the Plasticity lab at the NDCN. More about Anna on her [profile page](https://www.ndcn.ox.ac.uk/team/anna-guttesen). 

*"Our role as scientists is to systematically build knowledge – a task impeded by the lack of openness and transparency. This is what motivates me to incorporate open research practices into my workflow. As an Ambassador, I am excited to learn from the Open WIN community and to help promote an open research culture both within and outside the WIN."*

### Lara Nikel
Lara is a DPhil student at NDCN. More about Lara on her [profile page](https://www.ndcn.ox.ac.uk/team/lara-nikel).

*"I‘m very excited to join the ambassador team and help build an open science community within the department."*

### Lilian Weber
Lilian is a postdoc in the MoDeS lab in the Psychiatry Department. See her [profile page](https://www.psych.ox.ac.uk/team/lilian-weber/).

*"I want science to be more transparent, reproducible, accessible, and inclusive. I am enthusiastic about the possibility of contributing to the widespread use of open research practices across WIN and learn more about the tools other people are using to achieve these goals."*

### Ying-Qiu (Akina) Zheng
Ying-Qiu (Akina) is a postdoctoral researcher in the Nuffield Department of Clinical Neurosciences. More about Ying-Qiu on her [WIN profile page](https://www.ndcn.ox.ac.uk/team/ying-qui-zheng).

*"Realising the gaps in open science, particularly the dearth of reproducibility instructions in many worthy studies, I am eager to advocate for more robust open science practices, ensuring research is not just shared but is truly accessible and actionable."*

---

## The 2022-2023 Ambassadors

![Open WIN Ambassadors 2023]({% link img/img-ambassadors_2023.png %})
{: .fs-3 .fw-300 }

### Peter Doohan

Peter is a DPhil Student in the Nuffiled Department of Clinical Neuroscience. More about Peter on his [WIN Profile page](https://www.win.ox.ac.uk/people/peter-doohan).

*"I'm excited to learn new open science practices, apply them to my own work, and share them with the WIN community. I'm also interested in developing standardised data management methods for mutlimodal rodent experiments."*

### Miguel Farinha

Miguel is a DPhil Student in the Department of Psychiatry.

*"As an avid user of open-source software/data, I am very enthusiastic about the possibility of contributing to the widespread use of open research practices across the WIN community and the wider scientific community. "*

### Juju Fars

Juju is a Postdoctoral Researcher in the Nuffiled Department of Clinical Neuroscience. More about Juju on their [WIN profile page](https://www.win.ox.ac.uk/people/julien-fars)

*"I always wanted to make my analyses, data and experiment freely accessible for other research teams. Making it easy for others to reproduce my results is extremely important. "*

### Lisa Spiering

Lisa is a DPhil Student in the Department of Expereimental Psychology. More about Lisa on her [Department profile page](https://www.psy.ox.ac.uk/people/lisa-spiering)

*"I am very keen to learn more about the Open WIN tools that are available to us at WIN in order to make our research more transparent and replicable. I am also interested in learning more about how to promote an open and inclusive research culture. I've noticed that more researchers in my lab are interested in making their projects more transparent and open; so I am hoping that by becoming an Ambassador I can also help others to be more proficient in open science practices."*

### Mohamed Tachrount

Mohamed is a Senior Physics Support Scientist (Clinical and Pre-clinical) in the Nuffiled Department of Clinical Neuroscience. More about Mohammed in his [Department profile page](https://www.ndcn.ox.ac.uk/team/mohamed-tachrount)

*"I would like to learn how to make our scientific research more transparent, accessible, reproducible, and robust so any experiment (including protocols and data) can be checked, replicated, and extended by the community."*

-------

## The 2021-2022 Ambassadors

![Open WIN Ambassadors 2022]({% link img/img-ambassadors-2022-horizontal.png %})
Open WIN Ambassadors 2021-2022 (left-right): Dejan Draschkow; Yingshi Feng; Verena Sarrazin; Bernd Taschler.
{: .fs-3 .fw-300 }

### Dejan Draschkow

Dejan is a Departmental Lecturer at the Department of Experimental Psychology. See his profile [here](https://www.psy.ox.ac.uk/people/dejan-draschkow).

*"I applied to the Ambassadors programme to learn about WIN’s open science activity, provide a bridge to the work happening in the Department of Experimental Psychology and help disseminate best practice. The most valuable component so far has been staying in touch with Cass and staying up-to-date on the work that is being done."*

### Yingshi Feng

Yingshi is a DPhil student in WIN Plasticity Group. See her profile [here](https://www.win.ox.ac.uk/people/yingshi-feng).

*"I applied to the Ambassadors programme to learn more about strategies for making scientific research more replicable, rigorous, and accessible. I have gained skills in using data sharing tools and an awareness of the broad beneficial impact open science practices have on individual researchers, research groups, collaborators, and the general public."*

### Verena Sarrazin

Verena is a DPhil student in the Psychiatry Department. See her profile [here](https://www.psych.ox.ac.uk/team/verena-sarrazin).

*"I applied to the Ambassador programme because I wanted to meet other people who are interested in open science, discuss challenges and future directions, and help make implementation of open science as easy as possible for researchers. I found it very useful to learn to use git because it is so transferrable, and it was interesting to shift from “consumer” to “contributor” in developing documentation. I now feel more confident starting conversations about open science and suggesting that others to publish all of their research outputs."*

### Bernd Taschler

Bernd is a postdoc in WIN's Analysis Group. See his profile [here](https://www.win.ox.ac.uk/people/bernd-taschler).

*"I wanted to take part in the Ambassadors programme to make a direct contribution to changing the way we conduct and incentivise research. Working on a larger project as a team has been valuable in helping me practice my communication and collaborative development skills. I’m now more convinced than ever that open science practices will be fundamental in improving our current research culture.”*
