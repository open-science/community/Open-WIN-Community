---
layout: default
title: Apply to be a 2025 Ambassador
parent: Open WIN Ambassadors
grand_parent: Open WIN Community
has_children: false
nav_order: 7
---
# Application procedure
{: .fs-9 }

Find out about how to apply to the Ambassadors programme

{: .fs-6 .fw-300 }

---

**Contents**
- [How to apply](#how-to-apply)
- [How your application will be assessed](#how-your-application-will-be-assessed)
- [Timeline](#timeline)
- [Acknowledgements](#acknowledgements)

Below we describe the application process, including how your application will be assessed.

**We appreciate that the formality of this procedure may appear intimidating! Please be assured that the application is designed simply as a way to get to know you, and to gauge your current level of understanding and ideas. There are no right or wrong answers :). We have been intentionally transparent with our review process to make sure that we can judge all applications fairly.**

Please take note of the [timeline](#timeline). If you apply to the programme, please keep the date of the first meeting free in your schedule.

Please review the notes on [eligibility]({% link docs/ambassadors/logistics.md %}) and [time commitment]({% link docs/ambassadors/expectations.md %}) before applying.

When you apply you will be asked to confirm that you have discussed your application with your supervisor or line manager and that they are supportive of your participation in the Ambassadors programme.

### Application questions

<!--
APPLICATIONS for 2025 are now open! 
{: .label .label-yellow }

**To apply, please complete the application form here: [Apply now!](https://forms.office.com/e/R07RTZ4NUq)**
-->

<!--
*APPLICATIONS for 2025 will open soon!*
-->

*APPLICATIONS for 2025 are now closed! - If you are interested in getting involved in any open science projects please get in touch at open@win.ox.ac.uk!*

The above linked application form asks 3 questions to assess your interest and intentions for becoming an Ambassador. You will be asked to respond to each of the following in 100-150 words:

1. Why are you interested in becoming and Ambassador?
2. What will you contribute to the Ambassadors team and wider Open WIN community?
3. What will you gain from being an Ambassador?

Please note that no prior experience with open science related projects or activities is required!

You will then be asked to let us know about any logistical constraints you might have with participation according to our [proposed schedule]({% link docs/ambassadors/programme.md %}).

We would like our Ambassadors to represent the full diversity of our community. We therefore ask applicants if they consider themselves to be a member of one or more historically excluded groups. We will also ask for your career stage, to ensure we get diverse representation.

Your data will be stored in University approved systems (One Drive and MS Teams). Your de-identified data will be shared with members of the Open Neuroimaging Project Steering group for the purposes of scoring your application.

## How your application will be assessed

Your application will be reviewed by members of the [Open Neuroimaging Steering Group](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/#open-win-steering-group).

### Assessment rubric

Each of the "interests and intentions" questions will be given a score between 1 and 3, where 3 = criteria fully met, 2 = criteria partially met, and 1 = criteria not met. To avoid personal bias, each of these scores is clearly defined for every question in the application review form as below. This rubric aims to evaluate the application across multiple aspects and avoid any personal bias which reviewers may have.

Reviewers will consider the following questions:
1. Has the applicant answered the application questions?
2. Does the applicant have a clear, feasible, and relevant idea of how they will contribute to the Open WIN community?
3. Is the applicant enthusiastic about open science and the aims of Open WIN Community?

These positions will be scored by reference against the following descriptors:

| Sections | Score 1 | Score 2 | Score 3 |
|---|---|---|---|
| **Readiness for the Ambassador programme:** | (not ready) Does not provide enough information or seems to misunderstand the nature of the Open WIN Community and the Ambassador programme. | (enthusiastic) Seems to have a clear understanding of Open WIN Community and the Ambassador programme, brings along a specific skill or content for contributions. | (clear) Seems to have a clear understanding of the Open WIN Community and the Ambassador programme and a clear understanding of how they can contribute and collaborate with others. |
| **Goals for the contributions:** | (not ready) Shares vague or general ideas that are unrelated to the Open WIN Community and the Ambassador programme, or no goals at all. | (enthusiastic) Shares clear, overly ambitious ideas for the Open WIN Community and the Ambassador programme that can likely be refined in a brainstorming session. | (clear) Shares clear, achievable contribution/development ideas for the Open WIN Community Ambassador programme that fit goals and mission and are likely to be achieved through the applicant’s participation. |
| **Purpose of participation and what they will get out of the Ambassador programme:** | (not ready) Purposes for participation in the Ambassador programme seem almost entirely self-centered and about the applicant’s status, rather than about participating in Open WIN Community to develop the project. | (enthusiastic) Purposes for the participation in the Ambassador programme are not completely clear from the application or are limited (even though useful), such as typo or bug fixing. | (clear) Purposes for participation in the Ambassador programme are valuable in many ways and are likely to help the applicant to become an active contributor to the Open WIN community and take ownership of their work in the broader open science ecosystem in their own right. |
| **Willingness to collaborate and contribute after the programme:** | (not ready) Seems closed to collaborative ways of working or more interested in only one aspect of outputs, research or related topic. | (enthusiastic) Seems excited to learn from others and Open WIN project, but in a general way without much understanding of what those things mean yet. | (clear) Seems excited to collaborate with others and is motivated to contribute to the Open WIN community. |

*Applicants who score mostly 1s do not have a clear understanding of the overall goals of Open WIN Community.*

*Applicants who score mostly 2s are enthusiastic if not wholly suitable for the Ambassadors programme, for example, they may come with some ideas for a contribution which are not in the scope of the programme or community.*

*Applicants who score mostly 3s are clearly ready, goal-oriented, interested in contributing to the project, and excited to learn from others in the programme and community.*

The reviewers will provide a 1-2 sentence summary about the application to justify the scores received. They will also highlight any diversity characteristics which were disclosed. These will be combined with the "mostly 1s, 2s or 3s" decision above, and the reviewer will make a final decision as to whether they recommend the applicant for the programme ("Yes", "No", "Unsure"). Any applications which are graded as "Unsure" will be put to another member of the Steering Group for evaluation following the same rubric. Four to six applicants will be invited to participate in the programme.


## Timeline

- **Applications open**: now
- **Applications close**: 30th November 2024
- **Applications reviewed**: early-December 2024
- **Ambassadors informed of outcome**: early-December 2024
- **First meeting**: Tuesday 10th December 2024, 09:30-11:00, on Teams
- **Regular meetings**: Tuesdays AM (TBC)

## Acknowledgements

These application questions, scoring rubric and explanation of diversity selection are adapted from the [Mozilla Open Leaders](https://foundation.mozilla.org/en/initiatives/mozilla-open-leaders/), [Open Life Science](https://openlifesci.org) and [Turing Way Book Dash](https://the-turing-way.netlify.app/community-handbook/bookdash.html) programs.
