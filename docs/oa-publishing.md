---
layout: default
title: Open Access Publishing
parent: Publishing and Licencing
has_children: false
nav_order: 2
---

# Open Access Publishing
{: .fs-9 }

Find out about the Wellcome open access publishing policy which applies to all WIN researchers
{: .fs-6 .fw-300 }

---

The Wellcome open access publishing policy, as well as all steps necessary to follow this poilcy, have been summarized on the [WIN Open access policy](https://www.win.ox.ac.uk/research/publications/open-access-policy) webpage.

The full policy can be found on the [Wellcome Trust website](https://wellcome.org/grant-funding/guidance/open-access-guidance/open-access-policy). The [Bodleian OA website](https://openaccess.ox.ac.uk/wellcome) also has detailed information about how to comply.

To easily identify journals or platforms that enable Open Access compliance check the [Journal Checker tool](https://journalcheckertool.org/). This tool is not yet available but will be later this year. Some journals, such as Science, Elsevier, and Cell Press, are intending to become compliant but are not yet committed.

On another note remember that <ins>ALL ORIGINAL RESEARCH ARTICLES</ins> that are funded by the Wellcome, either partially or wholly, must include the rights retentions text below.
'This research was funded in whole, or in part, by the Wellcome Trust [Grant number xxxxx]. For the purpose of open access, the author has applied a CC BY public copyright licence to any Author Accepted Manuscript version arising from this submission.'
This text must always be included in the cover letter (or an equivalent section during the submission process) and the acknowledgements section WHEN you submit your paper. This cannot be added after you have already submitted.

If you have any questions or would like to discuss your individual case, feel free to contact:
- the [WIN OS team](mailto:open@win.ox.ac.uk)
- Kaitlin Krebs and Iske Bakker at [admin@win.ox.ac.uk](mailto:admin@win.ox.ac.uk)