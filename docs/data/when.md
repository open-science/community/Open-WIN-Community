---
layout: default
title: When should I think about sharing?
parent: Open Data
has_children: false
nav_order: 3
---


# When should I think about data sharing?
{: .fs-9 }

The stage of your project will add constraints to your data sharing plans
{: .fs-6 .fw-300 }

---

Sharing data can be hard if you have not built in the intention to share from the outset. In each of the project stages below, we suggest actions you can take to ensure your data sharing is efficient and stress free!


<!-- Coming soon
{: .label .label-yellow } -->
<br>

| Stage                                                  | What to think about now
|:-:|:--|
|  ![cani-reuse]({% link img/img-when-planning.png %}) **Planning**     | When your project is in the planning stage, you can think think carefully about the [ethical](can-i#ethics), [governance]({% link docs/data/can-i.md %}) and [identifiability]({% link docs/data/can-i.md %}) issues which might limit your ability to share your data completely openly. Once you know the limits of what is theoretically possible, you can describe how you can achieve this practically in a data management plan.     |
|  ![cani-reuse]({% link img/img-when-data-coll.png %})  **Data collection**   | When collecting your data you should aim to work with non-proprietary file file formats where possible. You can also begin to [collate metadata and organise your data according to community standards]({% link docs/data/can-i.md %}). |
|  ![cani-reuse]({% link img/img-when-anal.png %}) **Analysis**          | While you are analysing your data, be mindful of the restrictions which might be necessary to ensure the shared data are maximally [deidentified]({% link docs/data/can-i.md %}). For example, it might be preferable to conduct your analysis using binned categorical data rather than continuous variables which make individual participants more identifiable. Consider writing your code in a way that allows for flexible and efficient re-analysis on any such factors. You should also keep an active [data dictionary](https://faircookbook.elixir-europe.org/content/recipes/interoperability/creating-data-dictionary.html), where you describe the variables you create, how they are named in your code or data, and how they are derived. |
|  ![cani-reuse]({% link img/img-when-writing.png %}) **Writing up**      | While writing up your project, think about the reason you are sharing your data. If you are sharing data so another researcher can validate your figures, focus on ensuring the data (and code) required for that purpose are accessible and well described. If you are sharing a larger or more complete data set, you may wish to focus on ensuring that the value in the reuse of that data is appropriately emphasised and 'advertised' in your project write-up. Where the time and resources allow, you may wish to or consider publishing a separate [data paper](https://the-turing-way.netlify.app/reproducible-research/rdm/rdm-article.html). |
|  ![cani-reuse]({% link img/img-when-submission.png %})  **Publishing**  | You should aim to have all of your data available in your [intended repository]({% link docs/data/how.md %}) *before you submit your project manuscript to a journal for publication*. Your repository may have specific metadata requirements (much like journal formatting guidelines) and in some cases the data may need to be reviewed before it is made public (for example [EBRAINS](https://ebrains.eu)), so sharing it may take some time. By having the data available in advance of submission, you can include a persistent digital object identifier (doi) or reviewer-only access link with your manuscript. Ensure that you have carefully considered [how contributors to your data will be recognised]({% link docs/data/can-i.md %}).|
