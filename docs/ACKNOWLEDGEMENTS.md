---
layout: default
title: Acknowledgements
parent: Home
has_children: false
nav_order: 4
---

# Acknowledgements
{: .fs-9 }

We are grateful to the following resources for inspiration and shared documentation used to develop this repository
{: .fs-6 .fw-300 }

---

All the documentation created/modified between 2019 and 2022 is to put to the credit of [Cassandra Gould Van Praag](https://www.turing.ac.uk/people/researchers/cassandra-gould-van-praag) 

Materials within this repository were developed with the support of the [Open Life Sciences (OLS) Mentoring Program](https://openlifesci.org), with the kind assistance of [Naomi Penfold](https://github.com/npscience). 

The community strategy has been developed using materials shared by the [Center for Scientific Collaboration and Community Engagement - CSCCE](https://www.cscce.org) - community of practice. Guidance in community management and support has been generously provided by members of CSCCE. 

Other resources have also been developed following example materials developed by [The Turing Way](https://the-turing-way.netlify.app/welcome). We are particularly grateful to the Turing way for the publication of the [Book Dash Application and assessment rubric](https://the-turing-way.netlify.app/community-handbook/bookdash/bookdash-application.html), which we sought to replicate as a model of best practice in inclusive and transparent narrative based decision making.

`The Turing Way Community, Becky Arnold, Louise Bowler, Sarah Gibson, Patricia Herterich, Rosie Higman, … Kirstie Whitaker. (2019, March 25). The Turing Way: A Handbook for Reproducible Data Science. Zenodo http://doi.org/10.5281/zenodo.3233853.`

The Ambassadors scheme has been modelled on materials available from the [TU Delft Data Champions program](https://www.tudelft.nl/en/library/current-topics/research-data-management/r/support/data-champions/). 

The Git and GitLab tutorials have been recreated from material developed for the Open Life Sciences (OLS-2) cohort training session on using GitHub for collaborative documentation.

`Cassandra Gould van Praag. (2020). cassgvp/git-for-collaborative-documentation: OxBe 2020 (v1.0.1). Zenodo. https://doi.org/10.5281/zenodo.4058254`

We promote the use of the [Tenzing webapp](https://rollercoaster.shinyapps.io/tenzing/) for creating [CRediT](https://casrai.org/credit/) author statements.

`Holcombe, A. O., Kovacs, M., Aust, F., & Aczel, B. (2020). Documenting contributions to scholarly articles using CRediT and tenzing. Plos one, 15(12), e0244611.`

Figures have been created from images in the [Noun Project](https://thenounproject.com) repository, contributed by [Alice Design](https://thenounproject.com/rose-alice-design/), [Andrejs Kirma](https://thenounproject.com/andrejs/), [DinosoftLab](https://thenounproject.com/dinosoftlab/), [Luis Prado](https://thenounproject.com/Luis/), [Sergey Demushkin](https://thenounproject.com/mockturtle/), [Thuy Nguyen](https://thenounproject.com/milkghost/), [vectlab](https://thenounproject.com/vectlabmail/), and [Victor Llavata Bartual](https://thenounproject.com/victor.llavata/). 

The pages are regularly updated by the [Open Science Ambassadors]({% link docs/community/ambassadors.md %}). The last major update was made on June 2024 by [Anna Guttesen](https://orcid.org/0000-0003-0284-1578), [Lara Nikel](https://www.ndcn.ox.ac.uk/team/lara-nikel), [Lilian Weber](https://orcid.org/0000-0001-9727-9623), [Ying-Qiu (Akina) Zheng](https://orcid.org/0000-0003-1236-0700), [Juju Fars](https://orcid.org/0000-0001-7771-5029), [Bernd Taschler](https://orcid.org/0000-0001-6574-4789), [Yingshi Feng](https://orcid.org/0000-0001-9065-4945).
We thank them for all the work they have put to make this possible. 
