---
layout: default
title: Contributing
parent: Home
has_children: false
nav_order: 3
---

# Contributing
{: .fs-9 }

How to contribute to this repository
{: .fs-6 .fw-300 }

---

This repository is designed to be used by members of the Wellcome Centre for Integrative Neuroimaging (WIN), and we expect most contributions will come from WIN members. **If you are not a member of WIN, we still welcome your feedback and input!** Please contact open@win.ox.ac.uk to discuss how you can be involved.

---

We want to ensure that every user and contributor feels welcome and included in the WIN Open Neuroimaging community. We ask all contributors and community members to follow the [code of conduct]({% link docs/community/CODE_OF_CONDUCT.md %}) in all community interactions.

**We hope that this guideline document will make it as easy as possible for you to get involved.**

Please follow these guidelines to make sure your contributions can be easily integrated in the projects. As you start contributing to the WIN Open Neuroimaging, don't forget that your ideas are more important than perfect [pull requests](https://opensource.stackexchange.com/questions/352/what-exactly-is-a-pull-request).

## 1. Contact us
If you have ideas concerning the community pages or the tool you would like us to talk about, you can contact us using this email address: [open@win.ox.ac.uk](open@win.ox.ac.uk).
If you are a WIN member you can join us on the slack channel of the WIN Community slack workspace (coming soon!!!)

## 2. Check what we're working on

We invite you to review our [roadmap]({% link docs/community/roadmap.md %}) and see how we are progressing against our community milestones.

## Or, 3. Jump straight in!

If you are confident using git, markdown, and GitLab pages, you are very welcome to [submit an issue to our GitLab repository](https://git.fmrib.ox.ac.uk/open-science/community/Open-WIN-Community/-/issues), and submit a merge request.

### New to GitLab?
If you'd like to try out GitLab at your own pace, take a look at the [training materials available here]({% link docs/gitlab.md %}).
