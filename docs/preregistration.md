---
layout: default
title: Preregistration
has_children: false
nav_order: 8
---

# Preregistration
{: .fs-9 }


---

## Why preregister my study?
Preregistration separates hypothesis-generating (exploratory) research from hypothesis-testing (confirmatory) research. Both of these are important for the research cycle, however, the same data cannot be used to generate and test a hypothesis. Preregistration is a time-stamped document where you specify your hypotheses, data collection procedures and analysis plans prior to collecting and/or analysing the data. It helps remind ourselves and others what we set out to do prior to seeing the data, and what we explored after seeing the data. It can, for example, help reduce hypothesising after results are known (also known as HARKing). 


## How do I preregister my study?
One method of preregistering your study is through the Open Science Framework (OSF) portal. You can download a template registration form from their webpage, start writing your study plans, send your draft to collaborators, and, finally, fill in the form on the OSF page.
Get more information on preregistering using OSF by following this link: [https://help.osf.io/article/158-create-a-preregistration](https://help.osf.io/article/158-create-a-preregistration) 


## What is the difference between a preregistration and a registered report?
Put simply, a preregistration is a detailed time-stamped document of your study plans prior to data collection and/or analysis, while a registered report takes that one step further and is **a manuscript that undergoes peer review and in-principle acceptance** prior to data collection and/or analysis. Once the in-principle acceptance is granted, you collect and analyse your data, and you are in-principle guaranteed a publication irrespective of the results (as long as you do what you said you were going to do). In addition to reducing HARKing, etc, registered reports help combat, for example, the file drawer problem, as your study will be published no matter what your results look like.


## Video from WIN workshop on preregistrations/registered reports
More information coming soon
{: .label .label-yellow }