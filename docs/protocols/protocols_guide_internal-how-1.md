---
layout: default
title: Add a new protocol
parent: Open MR Protocols
has_children: false
nav_order: 2
---


# Adding a new protocol entry to the database
{: .fs-9 }


---

<!-- ![open-protocols]({% link img/img-open-mrprot-flow.png %}) -->


## Contents
- [1. Prepare the required files](#1-prepare-the-required-files)
- [2. Upload](#2-upload)
- [3. Top level protocol information](#3-top-level-protocol-information)
- [4. Attachments](#4-attachments)
- [5. Sequence level protocol information](#5-sequence-level-protocol-information)
- [6. Save](#6-save)
- [7. Set visibility](#7-set-visibility)
- [8. Reference List](#8-reference-list)


## 1. Prepare the required files

### 1.1 Ask your radiographer for your protocol pdf and scanning procedure file.
Your scanning procedure file (.doc) and scanner protocol (.pdf) will be given to you by your radiographer at the start of your project. You will be provided with a new version if there are changes in the protocol. You can request copies of these files by emailing radiographers@win.ox.ac.uk and providing your calpendo project number.

### 1.2 Request permission from the radiographer author of the scanning procedure to add them as an author on the public doi record of your protocol.
The radiographer author of your scanning procedure is listed at the top of the file. Contact this radiographer and ask their permission to be listed as a contributor to this protocol on the public doi record. Request the radiographers ORCID ID for inclusion.

### 1.3 Redact confidential information from the scanning procedure and convert it to a pdf.
Any names should be redacted from the scanning procedure unless permission to be listed has been provided by those individuals. Review the complete scanning procedure document (including instructions such as "check voxel location with <NAME>") and redact all identifiable information which you do not have permission to share. These details can be replaced with the word "REDACTED" or covered with a black rectangle.

Review all images and ensure that identifiable information such as participant IDs are redacted (cover with a black rectangle). Ensure identifiable facial features (in sagital views of slice pack positioning) are redacted (cover with a black rectangle).


When redacting MRI images, ensure the images are edited *destructively* (i.e., irreversibly).
The best way to ensure that an image has been edited destructively is by modifying the original image in a program such as Adobe Photoshop, or GIMP, and saving in a format like PNG or JPEG.

{: .warning}

Do not redact MRI images by dragging a black box over them in Microsoft Word, then exporting to PDF.
The unredacted image is still present in the file, and can be very easily extracted.

Currently, only the owner of the project is able to edit the uploaded protocol. If you would like to suggest changes to someone else's protocol you can do so by emailing the admin team who can then contact the project's owner.

You might want to add a statement such as "available for re-use, but I'm unable to assist with implementation" in the usage notes to clarify the extent of assistance you are able to provide.

## 2. Upload

### 2.1 Log in to the database.

Only WIN members (as determined by SSO authentication) are able to deposit protocols in the database.

![gif-log in]({% link img/img-protocols/gif_login.gif %})

To log in:
1. Go to [https://open.win.ox.ac.uk/protocols/](https://open.win.ox.ac.uk/protocols/)
2. Click the `Login` icon on the top right of the screen
3. Click the `Login via Shibboleth` option and enter your Oxford SSO details when requested.



### 2.2 Add a new protocol entry

Protocols are saved to individual user profiles.

![gif - add new protocol]({% link img/img-protocols/gif_add-protocol.gif %})

1. Once you are logged in, you will see the `Add Protocol` icon.
2. Click on `Add Protocol`, and you will be taken to the `Add new protocol` page.
3. Click the `Browse` button to open a file explorer.
4. Navigate in the file explorer to the scanner protocol (.pdf) you want to upload. Select `open` in your file explorer.
5. Click on the `Add new protocol` button to upload the selected pdf to the database.



### 2.3 File type error
If the uploaded file is recognised as a sequence pdf, you should see the `Register Protocol` page. If the file is not in the right format, you will see an error. Please check the file you have selected is a pdf of your MR protocol.

## 3. Top level protocol information
You can enter details about your protocol which are relevant to the study as a whole, or all sequences. These details are entered at the top of your protocol. These details will help future users to understand your decisions and/or implement your protocol.

NOTE: If you are publishing customised pulse sequences, please be sure to indicate your willingness and requirements for sharing them (see [3.4.1 Custom Sequences](#3-4-1-custom-sequences) below).

![gif - top details]({% link img/img-protocols/gif_top-details.gif %})

### 3.1 Project, hardware and species
Some fields of this section are autmatically retrieved from your pdf and other ones have to be added by selecting the appropriate items from the dropdown menus like:
1. Project (select from Calpendo registered projects)
2. Scanner
3. Species
4. Post mortem (tick if appropriate)

### 3.2 Keywords
Keywords can be used by other researchers to find your protocol. Note that their search query will be matched to keywords only and not to the text in the body of the entry.

Start typing keywords to select from a built-in list. Consider adding keywords related to:
1. Type of sequences (e.g., "T1 structural")
2. Regions of interest
3. Participants or clinical conditions
4. Cognitive or behavioural functions
5. Tasks or stimuli properties


### 3.3 Description
This section should identify and highlight important features of the protocol like the study design, the methods, and the procedure. The abstract of radiographers' procedure should be sufficient.


### 3.4 Usage guidance
This section should provide high-level instructions for the use of this protocol. Where possible, we recommend including the following:
1. **Contact details** for anyone willing to discuss the re-use of the protocol. Note this information will be publicly available if your protocol is made public. Only include contact details where explicit permissions have been granted.
2. **How to cite** this entry including any references you would like to be cited in addition to this entry (see [Issuing a doi and licence](#issuing-a-doi-and-license) below).
3. **Your terms for reuse** (see [Issuing a doi and licence](#issuing-a-doi-and-license) below) including any specific terms around authorship before re-using this protocol for example: "We request that future users of this protocol contact the authors to discuss collaboration and authorship agreements related to the reuse". This may be particularly relevant for experimental protocols or sequences.

You may also like to include some practical guidance such as the total length of the scanning session, the staffing levels, or any peripheral equipment. You may refer the reader to the radiographers procedure file if this has been attached (see [4. Attachments](#4-attachments)).

You should also highlight non-standard software or sequence requirements such as customised pulse sequences (see [3.4.1 Custom Sequences](#3-4-1-custom-sequences)).

### 3.4.1 Customised Sequences
If you are publishing a protocol that uses a customised pulse sequence, please make it clear if you are happy to share it. If so, please include the following guideance:

"This protocol uses a customised pulse sequence and its author is willing to share it with other labs provided:

- The lab/hospital has the appropriate contracts and licences with the scanner's vendor that allow customised pulse sequences to be installed on their system.
- We have a version that is available for your platform [(e.g. we currently can only supply for VB11C and VB11E)]
- You are willing to arrange for a C2P agreement with Siemens for this sequence
- You are willing to arrange for a sequence transfer agreement between Oxford University and the interested institution to be signed.

If all of the above apply then please contact [email address if the PI] in the first instance.  Please note that neither Siemens nor University of Oxford makes no commitment to support this transfer".

If you are not able to share the customised sequence, please include the following guidance:

"This protocol uses a customised pulse sequence.  We do not have the resources to share the sequence with other labs, so the protocol is only provided for your information."

## 4. Attachments
Browse to and add any attachments to support the implementation of your protocol. This should ideally include the (redacted) radiographers procedure. You may additionally choose to attach analysis of pilot data, for example.

*Note that there is a file size limit of 15MB for each individual file attached.*

## 5. Sequence level protocol information
You can add details specific to individual sequences. These details may make the understanding of the outputs easier for other users. You can include information about why these sequences where chosen or refer to decisions made during piloting. You may reference back to earlier versions of the protocol entry to indicate changes where appropriate.

Each of the sections below should be completed for each sequence.

![gif - sequence details]({% link img/img-protocols/gif_sequence-details.gif %})

### 5.1 Sequence name
This is identified from the pdf. You can update it or add any additional context where necessary.

### 5.1 Sequence keywords
Keywords including for example modality, ROIs, clinical conditions, type of study and population.

### 5.2 Sequence description
It provides key features that might help someone to decide whether the sequence meets their needs or not. For example, “A standard T1-weighted structural scan of 1mm isotropic resolution is suitable for registration to a standard template in an fMRI protocol".

It may additionally be useful to disambiguate any abbreviations in the name of the sequence, for example "bold_mbep2d_MB4P2_task_faces" could be described as "multiband 2D echo-planar sequence; multiband acceleration factor 4; PAT (GRAPPA) factor 2; for faces task.".

For fieldmap sequences, it may be useful to say which BOLD sequence they will be used to correct.

Where a specific task is mentioned, you could add the link to where that task is shared or published analysis of any data using that task.

Consider adding specific purpose and pros/cons of the sequence. For example, one sequence has a high temporal resolution but relatively low spatial resoltuion or it has been developed to focus on a specific brain area. Cons may include dropout or poor signal from certain regions.

## 6. Save
Once all required fields have been completed, you will be able to save the entry to the protocols database. Press `Save` to progress and return to the list of your protocols.


## 7. Set visibility
By default, your entries will be set to `Internal (University Only)` visibility when you add them to the database. This means users will be required to log into the the protocols database with an SSO in order to review or download your entry.

After they have been entered onto the database you can make individual protocols publicly visible (`Open Access`) and identify them as `WIN Recommended` protocols as appropriate.

### Open Access Actions
If you choose to make a protocol public ("open access") ensure you have added a license note to your entry, reserved a doi for the protocol using zenodo, and added the doi to the usage guidance (see [Issuing a doi and licence](#issuing-a-doi-and-license) below).

If you identify a protocol as open access, this will trigger a review by the database's administrators. For any questions please [email open@win.ox.ac.uk](mailto:open@win.ox.ac.uk).

## 8. Reference List
This field features prominently at the top of the entry when it is accessed by users, so it may be appropriate to repeat any important citation details which are listed in the usage guidance.

Add any references which are relevant to this protocol for example the accompanying paper or pre-print. You can also use this space to draft the author list and track changes in contribution.

We recommend that you consider using the [CRediT](https://casrai.org/credit/) system for identifying individual author contributions. You may wish to order authors alphabetically.

![gif - references]({%link img/img-protocols/gif_references.gif %})
