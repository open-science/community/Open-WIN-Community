---
layout: default
title: Track engagement
parent: Open MR Protocols
has_children: false
nav_order: 5
---


# Tracking engagement
{: .fs-9 }


---

<!--![open-protocols]({% link img/img-open-mrprot-flow.png %})-->


You can track how many times your protocol has been viewed and downloaded from the `Engagement Statistics` panel. Views and downloads are separated into internal to Oxford (users have logged in with an SSO) or external. When reporting on the impact of your entry, remember to also include the Zenodo views and downloads statistics, if you have used Zenodo to create a DOI.
