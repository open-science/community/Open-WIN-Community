---
layout: default
title: Issue a doi and license
parent: Open MR Protocols
has_children: false
nav_order: 3
---


# Issuing a doi and license
{: .fs-9 }


---

 <!--![open-protocols]({% link img/img-open-mrprot-flow.png %}) -->


We suggest using the free tool Zenodo for creating a Digital Object Identifier (DOI) for your protocol. This will allow others to cite your protocol independently from other work in the study and add contributors who may not be named elsewhere.

#### Who to include as an author

This is an opportunity to provide attribution to all individuals who contributed to this project up to the point at which the scanning protocol was finalised and/or published. This may include individuals who contributed to each of the stages below:
- Gaining ethical approval
- Registry as a clinical trials or pre-registration
- Sequences' development and piloting the acquisition protocol
- Analysis of the pilot study data
- Authoring of the Radiographer's procedure

##### ORCID
Where possible, we promote the use of ORCID to consistently identify and attribute authors. Request your contributors to provide their ORCID for inclusion. Alternatively, you can look for them on the [ORCID database](https://orcid.org) but make certain that you have found the correct individuals.

##### CRediT
You may wish to consider assigning authorship using the [Contributor Roles Taxonomy (CRediT) notation](https://casrai.org/credit/), which is now formally recognised by major publishers. Using this standard, the contribution of each individual is clearly stated against pre-defined roles. This taxonomy is often paired with alphabetical ordering of authors rather than prioritising "first author" and "last author" positions.  

##### Tenzing
You can write a CRediT statement by hand, or you can use a tool to curate the required information. The [Tenzing app](https://rollercoaster.shinyapps.io/tenzing/) can use a csv table of the author's information to generate CRediT statements in multiple different forms.

You can collect the author's information (for example ORCID, affiliations, funding acknowledgements) using the google table linked via Tenzing (see [How to use tenzing](https://rollercoaster.shinyapps.io/tenzing/)). Alternatively, you are welcome to take a copy of this MS Form: [Authoring information for CRediT statement (Open WIN)](https://forms.office.com/Pages/ShareFormPage.aspx?id=G96VzPWXk0-0uv5ouFLPkYMD50Te0q5HobQjqRFNJmpUM0NHR01CTkxZV0dTMUxLR0hQOTJGTURUTC4u&sharetoken=2cQ8XQ8eqkhqEhTRQT51) to collate your author's information. Then, send the form to each of your intended authors and use the information collected to generate the csv table required by tenzing.


#### Using Zenodo

The basic process below outlines how to upload a document to zenodo to generate a DOI. We have also provided boilerplate text suggestions for use where relevant.

Note once you publish your protocol with zenodo, it cannot be deleted. If you are doing this for the first time or you are uncertain of the process, Zenodo provides a practice sandbox website for testing. This can be found at [sandbox.zenodo.org](https://sandbox.zenodo.org/). The website interface is identical to the ‘real’ [zenodo.org](https://zenodo.org/), but the issued DOIs on the sandbox are not real. Note that you 1) have to make a separate account on the sandbox site; and 2) that there is no way of converting a sandbox listing to a real one. You have to replicate the steps you took on [zenodo.org](https://zenodo.org/).

1. Create a zenodo account using your ORCID ID or log into an existing zenodo account.
2. Go to `Upload` and select `new upload`.
3. Set the file type to `other` and **tick the** `reserve DOI` **box**
4. Enter a title. We suggest to use: "WIN MR Protocol: [your protocol name]"
5. Add authors as described above, including their ORICD.
6. Description: Suggested text below

    ```
    Protocol for the acquisition of MRI data for the study "[your protocol name]".

    Conducted at the Wellcome Centre for Integrative Neuroimaging (WIN), University of Oxford.

    Please see the entry of this protocol in the WIN MR Protocols Database to ensure this is the latest version: [your stable url, provided by the protocols database]

    Authors' list and CRediT roles: [your author list]

    Additional papers to be cited: [publications relating to this protocol, including preprints]

    Acknowledgements: [acknowledgements, for example funders]
    ```
7. Copy the **reserved DOI**, title and authors into your Usage Guidance section on the protocol database entry. It may be helpful to look at existing zenodo entries to see how this will be formatted, for example the zenodo entry for [2018_114 Seven Day Prucalopride](https://zenodo.org/record/6107725#.YhUSWy2cZBw).
8. Download your protocol entry from the WIN database in pdf form. If your entry contained attachments, zip them into a single file.
9. In the files section at the top, click `choose files` to locate your protocols database entry (pdf or zipped directory). Once located, click `start upload`.
10. Fill in any additional language, keywords or notes information as you wish.
11. Set your access requirements and choose a license as appropriate. We suggest a [CC-BY-4.0 license](https://creativecommons.org/licenses/by/4.0/)
