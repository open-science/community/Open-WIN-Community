---
layout: default
title: Why use the open tasks repository
parent: Open Tasks
has_children: false
nav_order: 1
---


# Why use the WIN Task Repository
{: .fs-9 }

Why you should consider using the WIN MR tasks repository.
{: .fs-6 .fw-300 }

---

 ![open-tasks]({% link img/img-open-tasks-flow.png %}) 

## Benefits
### Version control ![version-control]({% link img/icon-version-control.png %})
The [WIN Open Tasks repository](https://git.fmrib.ox.ac.uk/open-science) is maintained in the WIN GitLab server. WIN members are invited to deposit their task code and materials when they are using a stable version, by cloning from their own GitLab or GitHub repository.  This implies that WIN members will be using a git process when developing their task, and as such their development journey will be version controlled. Find out more about [using GitLab]({% link docs/gitlab.md %})

### Citable research output ![doi]({% link img/icon-doi.png %})
Versions of task materials can be assigned a digital object identified (DOI) using [Zenodo](https://zenodo.org) by uploading individual task repositories from GitLab. Once a DOI has been created, your task material becomes a citable object which you can add to your list of research outputs. Find out how to [create a doi for your repository]({% link docs/gitlab/4-2-you-doi.md %}).

### Reproducible methods detail ![reproduce]({% link img/icon-reproduce.png %})
WIN members will be supported in developing the necessary and sufficient documentation and running environments to enable others to launch, maintain and adapting their tasks. This facilitates effective re-use and modification of tasks within a research group, and with external collaborators.
