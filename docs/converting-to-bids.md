---
layout: default
title: Converting to BIDS
has_children: false
nav_exclude: true
nav_order: 1
---

# Converting to BIDS
{: .fs-9 }

sub heading
{: .fs-6 .fw-300 }

---

![BIDS]({% link img/logo-BIDS.png %})


The Brain Imaging Data Structure (BIDS) is a highly specified file naming format which is growing in use across neuroimaging communities. By converting our data into BIDS format, we are benefitting from many years of experience in determining the best way to structure our data to make it easily readable for both humans and machines. Applying the BIDS format also reduces friction in sharing our data with other neuroimagers, and applying code developed on other projects. We therefore improve the transparency and reproducibility of our research by converging on this community standard.

Read more about the BIDS format here: http://bids.neuroimaging.io

## Historic data
If you have historic dicom data, you can follow the process below to convert this into BIDS format images.
If you only have access to historic nifti data, you will need to write a custom script to convert the file names of your images into BIDS format. This may be more or less complicated depending on the variability of your file names and knowledge of sequence parameters, etc. Code for performing this conversion is available from BRC Imaging Support, but it may require a significant amount of modification to be used appropriately with your data.

## New data
Dicom data can be converted into BIDS format images manually (i.e. convert dicom to nifti, then rename your nifti files), or using code. While the manual conversion may appear easier in the first instance, you will achieve a greater efficiency and accuracy in your conversion if you use tested code. To this end, we will be using code developed by others to perform our BIDS conversion. A number of project exist to assist the conversion process (see https://neurostars.org/t/convert-data-to-bids-format/720 for a discussion). We have chosen to use heudiconv (https://github.com/nipy/heudiconv/blob/master/README.md), as this has strong evidence of continuing development and maintenance through active github collaboration.

Running heudiconv requires both bash and python scripts. For best reproducibility, we will run heudiconv in a Singularity container (https://en.wikipedia.org/wiki/Singularity_(software)) on jalapeno. The container was created from heudiconv version 0.5.3.

We have created a series of scripts to perform the conversion, and shared these via github. Below we will describe the process of accessing the scripts and modifying them for your data. Although you are not expected to understand everything in these scripts at present, you will benefit from reading through them, looking up functions you are not familiar with online, and making additional comments where appropriate.

Other resources accessed in creating this workshop guide:

We will be following the guide for heudiconv available here, with some minor changes to make your batch processing for all participants a bit less code heavy! http://reproducibility.stanford.edu/bids-tutorial-series-part-2a/

Useful info about ‘stacking’ of nifti files from multiple dicom sequences and running out of memory in (local) docker: https://github.com/nipy/heudiconv/issues/206

Neurostars topic on running heudiconv in singularity: https://neurostars.org/t/singularity-and-heudiconv/3588

Building a singularity container from a docker image: https://singularity.lbl.gov/quickstart

Basic interactions with a singularity container https://www.osc.edu/book/export/html/4678

Other usage of singularity for heudiconv (note this is for singularity 2.4, and we have singularity 3 installed on jalapeno): https://uosanlab.atlassian.net/wiki/spaces/SW/pages/45285423/Containers+Docker+Singularity

BIDS validator on dockerhub for creating the singularity image: https://hub.docker.com/r/bids/validator

Getting the bids-validator version number, after running the singularity container in a shell: https://neurostars.org/t/bids-validator-command-line/2411/3

### Download the code and singularity containers from gitlab
Go to https://git.fmrib.ox.ac.uk/cassag/h2mri/tree/master/BIDSConversion and click the download link on the right. Download the folder in .zip format.

Next locate the download in a file explorer (finder) and open (unzip) the zipped folder. We will copy the folder “BIDSConversion” up to your study code folder on jalapeno.

In your terminal (if you are using a Mac), locate the BIDSConversion folder (go to where you can see it, not into it) and enter the command below:
```
rsync -arvh BIDSConversion your-user-name@jalapeno.fmrib.ox.ac.uk:/vols/Scratch/brc_em/your-study-name/code/
```

If you are using Windows and do not have cygwin installed, you can transfer the folder to jalapeno using cyberduck.

### Open the code in Atom and update your git repository
Open Atom on jalapeno via a vnc connection and add your study code as a project folder. If you successfully linked gitlab version control to your code folder, gitlab should now show you that there are changes to be committed.
Throughout the following you are encourage to update your gitlab using the atom gui in the right panel after making significant changes from the original script.
Note there will be a number of places in these scripts which will need to be modified to work with your data. These have been identified with the comment “h2mri-update:”. You can search for all instances of this in the project using ctrl+shift+f.

### Create dicominfo files for all participants (BIDSConvert_01_generateDicomInfo_batch.sh)
The first script will create a dicominfo.tsv (table) for each of the participants listed in our dicom folder. The information in these tables will be used to identify the scans we want to convert to BIDS format.
In Atom, open the script BIDSConvert_01_generateDicomInfo_batch.sh and update the following to specify your study folder locations and participant ids:
1. “dataRoot” (line 11)
2. participant ids, currently specified as “011 012 013” (line 18)
3. your study folder, currently specified as “test” (line 35)
4. your study folder, currently specified as “test” (line 36)

Note that the first part of this script takes your dicom files out of their original folders and places them into a single file. This is heudiconv’s preference for handling dicom data. The dicom data will not be deleted, but it will not be easy to identify which files relate to each specific sequence, although you can still review the individual dicom sequences in fsleyes. If you want to work with individual sequences which are not currently covered by the BIDS format (e.g. ASL sequences), it may be easiest to re-download these data via Calpendo at a later stage.
Make this script executable by locating it in a terminal window and entering the command below:
```
chmod g+x BIDSConvert_01_generateDicominfo_batch.sh
```
Run the script from the same terminal window by entering the command below:
```
bash BIDSConvert_01_generateDicominfo_batch.sh
```
The important line(s) to look out for in the output are confirmation that heudiconv has run for the participant(s) you are working on. This line begins “INFO: Processing #### dicoms”. If the number is ~999+ (i.e. all your dicom files for this participant), it has worked. If the number is 0, it has failed. This usually means the path to the dicoms is wrong, or something straightforward.

### Select exemplar dicominfo.tsv files for heuristic (BIDSConvert_02_dicomInfoRef.tsv)
We will now use the dicominfo.tsv file created by heudiconv to create a rule (heuristic) for converting our dicom images to BIDS format. We will create this based on the dicom info from a typical or exemplar participant, as in these participants we can be sure that the first T1 scan generated is the correct one to use, for example. All the dicominfo.tsv files created in 7.2.3 above will be compared to this exemplar. If they are the same, the heuristic will run. If they are not, you will have to action them with a slight modification.

In Atom, add your study data folder to project. You will notice that a directories called BIDS and BIDS/.heudiconv have been created.

Identify from your records one participant in which the scanning session was completed exactly as planned (e.g. no spurious sequences). Open the dicominfo.tsv file for that participant, located in BIDS/.heudiconv/participantID/info/dicominfo.tsv (note that this file is tab separated, therefore you will need to select the /t option in tablr). Copy this dicominfo.tsv file and rename it to BIDSConvert_02_dicominfoRef.tsv. Move it to your code/BIDSConversion folder (overwriting the original, or perhaps renaming the original to something else). Later scripts will look for your reference as exactly BIDSConvert_02_dicominfoRef.tsv, so it is important that your file is named as such.

### Create the exemplar heuristic (BIDSConvert_03_heur_dicomInfoRef.py)
We will next use the reference dicominfo.tsv to create our heuristic.
Open the script BIDSConvert_03_heur_dicominfoRef.py. Enter your own variable names for your sequences (lines 29-56) and the correct formatting for sequences of that type (lines 29-39) based on the BIDS specification available at https://github.com/bids-standard/bids-starter-kit/wiki.  Refer to your dicom data dictionary on OSF to recall which sequences perform which function, e.g. which is a sbref BOLD image, which is bias field corrected T1w, which fieldmap is phase and which is magnitude, which fieldmap use to be used in the correction of which BOLD sequence.

### Run the exemplar heuristic (BIDSConvert_04_runHeur_dicomInfoRef_batch.sh,  BIDSConvert_04a_compareDicominfoToRef.py)
We will now run a bash script which conducts the heudiconv conversion using the specified heuristic created above (BIDSConvert_04_runHeur_dicomInfoRef_batch.sh). This script additionally calls a python script in each iteration of the loop which compared the participant and reference dicominfo.tsv files to determine whether the exemplar heuristic is appropriate (BIDSConvert_04a_compareDicominfoToRef.py).
Open the script BIDSConvert_04_runHeur_dicomInfoRef_batch.sh and update the following:
1. participant ids, currently specified as “011 012 013” (line 10)
2. your study folder, currently specified as “test” (line 38)
3. your study folder, currently specified as “test” (line 39)

Open the script `BIDSConvert_04a_compareDicominfoToRef.py` and update the dataRoot (line 14)
Make the bash script executable by locating it in a terminal window and entering the command below:
```
chmod g+x BIDSConvert_04_runHeur_dicomInfoRef_batch.sh
```
As this script calls some python, we will first activate our python environment so we can access the libraries we need.
```conda activate /vols/Scratch/brc_em/your-study-name/python_env/python_env/
```
Run the script from the same terminal window by entering the command below:
```bash BIDSConvert_04_runHeur_dicomInfoRef_batch.sh
```

### Review warnings and fix any non-typical participant sessions (BIDSConvert_04b_heur_2017-102-004.py, BIDSConvert_04c_runHeur_singlePx.sh)
If you have data which doesn’t match your exemplar format, you will receive a warning in the command window after you run the script above:
```
-- pxID: xxx
-- WARNING: Dicoms do not match reference. This dataset may require a uniquely specified heuristic.
```
These warnings will be hidden the results of the heudiconv output, so it is important to look through the output thoroughly.

For participants requiring a uniquely specified heuristic, examine their dicominfo.tsv file and determine where it deviates from the exemplar. Use your knowledge of the scanning session, and review of the dicoms to identify the correct sequences to be converted.

Open ```BIDSConvert_04b_heur_2017-102-004.py``` and update the sequence variable names and BIDS formatting key to match your exemplar conversion heuristic (lines 30-56). Update the sequence identifier information (lines 90-111) to correctly identify the appropriate dicoms to convert for this participant.

You may be able to create a heuristic which is appropriate for more than one participant. It may therefore be advisable to check all the participants which were not converted above and determine which could be processed with this new heuristic.

Save this new heuristic with an appropriate identifier, e.g. the id of the participant it was created for (“2017-102-004” in the example) or some other meaningful information.
Open ```BIDSConvert_04c_runHeur_singlePx.sh``` and update the following:

1. the name of your individually specified heuristic file (line 9)
2. the participant id(s) which this heuristic should be applied to, currently specified as “2017-102-004” (line 12)
3. your study folder, currently specified as “test” (line 46)
4. your study folder, currently specified as “test” (line 47)

Make the bash script ```BIDSConvert_04c_runHeur_singlePx.sh``` executable by locating it in a terminal window and entering the command below:

```
chmod g+x BIDSConvert_04c_runHeur_singlePx.sh
```

Run the script from the same terminal window by entering the command below:
```
bash BIDSConvert_04c_runHeur_singlePx.sh
```

### Fix fieldmap .json files (BIDSConvert_05_fixFmapJson.py)
.json files are created by heudiconv to assist in the BIDS formatting. They contain a great deal of information determined from the dicom series data, and are essential for downstream processing of the images, e.g. in fmriprep. We have detected a number of issues concerning the generation of filedmap .json files using heudiconv 0.5.3 (and the embedded dcm2niix) which will need to be fixed (if you have fieldmap data) before you have a valid BIDS data set.
Open BIDSConvert_05_fixFmapJson.py and update the location of your BIDS folder (line 21).

At the file.split function (line 43), we are splitting the file name (including the path) into separate strings (“outputs”) using an underscore as a delineator. You will receive an error if the number of expected outputs more than the number of outputs generated. Look carefully at your path for fmap/*.json files and amend the “x” placeholders to match the minimum number of outputs for all fmap/*.json files.

Update the BOLD acquisitions ("str_intendedFor") which these fieldmaps should be used with based on the labels which were created by your heuristic ("acq-type"). Check BIDSConvert_03_heur_dicominfoRef.py for reference (lines 51-60) and in the converted BIDS output (HINT: look out for multiecho sequences).

Run the script to correct json errors:
```
python BIDSConvert_05_fixFmapJson.py
```
If you get permission errors, check that the permissions automatically applied to the BIDS folder (and contents) are appropriate for write access for the user (and group where necessary).

### Attempt BIDS validation
You can check that you have successfully recreated the bIDS format for your data by uploading your BIDS directory to http://bids-standard.github.io/bids-validator/ (best accessed via firefox or chrome). This is not recommended for large datasets which have been created on jalapeno.

We have created a singularity container containing the latest version of the BIDS validator (version 0.0.0, node v8.11.3). This is a simple command to run so we will access it directly from a terminal.
1. Navigate to where you can see your BIDS folder.
2. Start the singularity container in a shell (note running the container will change the command prompt in your terminal):
```
singularity shell /vols/Scratch/brc_em/your-study-name/code/BIDSConversion/singularityContainer_validator_latest.sif
```
3. 	Navigate to where you can see your BIDS folder and run the bids-validator command:
```bids-validator ./BIDS
```

“Warnings” do not invalidate the BIDS format, but they should be attended to (some we will get to in later sections, e.g. filling in the participant event files).

Make a note of the items marked TODO (created by heudiconv) and complete this information. You can also find these fields by searching for “TODO” in Atom over your entire data directory.

Speak to BRC Imaging Support if you have systematic errors which need correction (we may need to develop code to address these).

Exit the singularity shell using ```exit```

Note the data available to the singularity shell is fixed from the point you open it. This means if you attempt to re-validate after making a correction to a file, the change will not be picked up by the shell or the validation. If you wish to re-validate, exit the shell, open it again and run the validator again.
