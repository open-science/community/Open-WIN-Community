---
layout: default
title: License & Citation
parent: Home
has_children: false
nav_order: 5
---

# License and citing this repository
{: .fs-9 }

How to reuse and credit resources in this repository
{: .fs-6 .fw-300 }

---

The materials contained within this repository are distributed under [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/legalcode).

**You are free to**:
- **Share**: copy and redistribute the material in any medium or format.
- **Adapt**: remix, transform, and build upon the material for any purpose, even commercially.

**As long as you**:
- **Attribute**:  must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

# How to cite these materials

Our citation recognises all those who have contributed to this project formally and informally. Named authors have directly contributed written material for the guide.

Please cite as:
*Open WIN Community, (2024), Open Win Community Pages - Updated version. Zenodo [https://10.5281/zenodo.11574171](https://10.5281/zenodo.11574171)*

For an access to the older version of the website (v2022): follow this link 
[https://doi.org/10.5281/zenodo.7463254](https://doi.org/10.5281/zenodo.7463254)
