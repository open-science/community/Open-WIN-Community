---
layout: default
title: Making your repository citable
parent: Git and GitLab
has_children: false
nav_order: 4
---


# Making your repository citable
{: .fs-9 }

How to create a doi in zenodo for your GitLab project
{: .fs-6 .fw-300 }

---

*Note this is a duplicate of [tutorial 4.2 Create a DOI]({% link docs/gitlab/4-2-you-doi.md %})*

Zenodo is free to use tool for creating a digital object identifier (doi) for your shared resources. A doi is essential for enabling other researchers to cite and reuse your material, and for you to receive proper attribution. Your doi entry will include your [ORCID ID](https://info.orcid.org/benefits-for-researchers/), so it can be tracked against all your other research outputs.

## First time creating a doi for a GitLab repository

1. In [zenodo](https://zenodo.org), log in or create an account. Go to "[new upload](https://zenodo.org/deposit/new)" and add details about your material. Click the "reserve" button to get the doi which will be issued by zenodo.
2. Copy this doi into your README for your project on GitLab, or other documentation. Include this in a section entitled "How to cite this material".
3. Make a “[release](https://stackoverflow.com/questions/29520905/how-to-create-releases-in-gitlab)” of the repository. Add a note to mark this as the initial release.
4. Download the repository from GitLab (download icon on the repository home page) as a .zip or other compressed file type.
5. Upload the compressed file collection to zenodo.
6. Finalise the zenodo entry with all contributors including their ORDIC IDs. Include a link to the GitLab repository.
7. Set an embargo period for the material if it is not yet publicly available on GitLab.

## Updating the material

If you make substantial changes to your material, you may want to update the doi record. Changes may include adding new contributors, or new code.

Zenodo allows you to issue a new "version" of your record. All versions will resolve to a single doi, therefore anyone who follows your old doi will see that a new version is available. Read [more about this from zenodo](https://blog.zenodo.org/2017/05/30/doi-versioning-launched/).

To create a new version of your record:

1. Make a “[release](https://stackoverflow.com/questions/29520905/how-to-create-releases-in-gitlab)” of your GitLab repository. Add a note to describe the changes that have been made.
2. Download the repository from GitLab (download icon on the repository home page) as a .zip or other compressed file type.
3. Go to your zenodo entry for the first release. Click the "new version" button and upload the new material. Make any other changes to the record as necessary.


This version updating process can be automated using the [zenodo API](https://developers.zenodo.org). This may be useful if you are making regular (or more complex) changes to your material. The method for doing this will reuse the material created for [funpak](https://git.fmrib.ox.ac.uk/fsl/funpack). Further guidance and a template for usage will be made available shortly.

## Practice makes perfect
If you are doing this for the first time, or are otherwise uncertain of the process, Zenodo provides a practice sandbox website for testing. This can be found at [sandbox.zenodo.org](https://sandbox.zenodo.org/). The website interface is identical to the 'real' [zenodo.org](https://zenodo.org), but DOIs issued on the sandbox aren't real. Note that you 1) have to make a separate account on the sandbox site; and 2) that there is no way of converting a sandbox listing to a real one, you will have to replicate the steps you took on [zenodo.org](https://zenodo.org).
