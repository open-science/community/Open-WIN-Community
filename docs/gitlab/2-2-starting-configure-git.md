---
layout: default
title: 2.2 Global git configuration
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 5
---


# Global git configuration
{: .fs-8 }

Configure git on your system (one time only)
{: .fs-6 .fw-300 }

---

You need to set a few variables so git knows who you are and who is making changes to your material. This is done once only for each user on the computer.

The easiest way to set up your configuration is to make a repository. This generates a series of instructions to follow which are unique to your account.

Create a new project in the GitLab web interface called “test-git-project” (keep it private and do not initialise a readme). The next page will then provide the instructions for configuring git on your computer for the first time.

Enter the “Git Global setup” commands into your terminal, one line at a time. Note if you are a Mac user, you may want to additionally configure git to [ignore the .DS_store files](https://www.jeffgeerling.com/blogs/jeff-geerling/stop-letting-dsstore-slow-you) which are created every time you browse a directory in finder.

You can then list the contents of your git config file using the command below, and check the above variables have been stored.


$ git config --list

