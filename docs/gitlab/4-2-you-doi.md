---
layout: default
title: 4.2 Create a doi
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 12
---


# Creating a doi
{: .fs-9 }

How to create a doi in zenodo for your GitLab project
{: .fs-6 .fw-300 }

---

Zenodo is free to use tool for creating a Digital Object Identifier (doi) for your shared resources. A doi is essential for enabling other researchers to cite and reuse your material, and for you to receive proper attribution. Your doi entry will include your [ORCID ID](https://info.orcid.org/benefits-for-researchers/), so it can be tracked against all your other research outputs.

## First time creating a doi for a GitLab repository

1. Navigate to the [login](https://zenodo.org/login/) page for Zenodo.
2. Log in or create an account. 
3. Go to "[new upload](https://zenodo.org/deposit/new)".
4. Select "No, I need one" and then click the "reserve" button to get the doi which will be issued by zenodo.
5. Copy this doi into your README for your project on GitLab, or other documentation. Include this in a section entitled "How to cite this material".
6. Make a “[release](https://stackoverflow.com/questions/29520905/how-to-create-releases-in-gitlab)” of the repository. Add a note to mark this as the initial release.
7. Download the repository from GitLab (download icon on the repository home page) as a .zip or other compressed file type.
8. Upload the compressed file collection to Zenodo.
9. Finalise the zenodo entry with the details of your project. For instance:
    - Select the Resource Type (e.g., Software/ Computational notebook).
    - Choose a license from the options.
    - Add all contributors including their ORDIC IDs.
    - In the Software section include a link to the GitLab repository.
    - Add details of your related work (to get more publicity for your previous work)
    - Set an embargo period for the material if it is not yet publicly available on GitLab.\
10. Save draft and/or publish.

#### Note:
Once the record is published you will no longer be able to change the files in the upload! However, you will still be able to update the record's metadata later.

## Updating the material

If you make substantial changes to your material, you may want to update the doi record. Changes may include adding new contributors, or new code.

Zenodo allows you to issue a new "version" of your record. All versions will resolve to a single doi, therefore anyone who follows your old doi will see that a new version is available. Read [more about this from zenodo](https://blog.zenodo.org/2017/05/30/doi-versioning-launched/).

To create a new version of your record:

1. Make a “[release](https://stackoverflow.com/questions/29520905/how-to-create-releases-in-gitlab)” of your GitLab repository. Add a note to describe the changes that have been made.
2. Download the repository from GitLab (download icon on the repository home page) as a .zip or other compressed file type.
3. Go to your zenodo entry for the first release. Click the "new version" button and upload the new material. Make any other changes to the record as necessary.


This version updating process can be automated using the [zenodo API](https://developers.zenodo.org). This may be useful if you are making regular (or more complex) changes to your material. The method for doing this will reuse the material created for [funpak](https://git.fmrib.ox.ac.uk/fsl/funpack).
