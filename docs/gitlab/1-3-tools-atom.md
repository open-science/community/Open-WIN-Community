---
layout: default
title: 1.3 Text editors
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 3
---

# Text editors
{: .fs-8 }

How to set up and use text editors for writing markdown
{: .fs-6 .fw-300 }

---

![logo-sublime]({% link img/logo_sublimetext_512.png %})


Sublime is my favourite text editor. Python, Bash, Markdown, C++, html... you name it. It can even handle Matlab! 

## Why I love Sublime

What I particularly love about [Sublime](https://www.sublimetext.com/download) is that I can switch between different languages very comfortably), and it takes care of everything the same way, no matter what you are writing. It handles the colours, the spacing, the formatting/syntax, [linting](https://en.wikipedia.org/wiki/Lint_(software)), the keyboard shortcuts, autocompletions, commenting, spell checking, searching / find+replace across multiple files, all the important stuff! You can also use the companion software "Sublime Merge" to use Sublime with Git. 

You must think "WOW, this is incredible! And is it Open Source?" 

Well, no it is not. 


Now you must wonder why I am advertising for Sublime on a website which advocate for open source. Well since Atom has been [discontinued](https://github.blog/2022-06-08-sunsetting-atom/), we must find alternatives that have comparable options.  
This is why, on this page I will list text editors that are, for me, good alternatives to Atom. 

Please contact us if you know other great alternatives :) 

## Why you should use Text editors

To host your documentation on GitHub pages, you're going to be writing across multiple documents, and potentially in more than one language. Best are text editors which also have a directory tree that enables you to navigate around your project file easily.

Good text editors also provide a graphical point-and-click way of committing changes to your GitHub repo. This can be useful if you're not familiar with the process, but for these tutorials we're going to stick to command line and the terminal for GitHub, because you get more informative error reports.

## Other good text editors 

If you want to use a good open science text editor (and you should!). You might want to try [Vim](https://www.vim.org/download.php). This powerful text editor is available on Windows, Linux and MacOS. Although git is not directly build in the software, you can find a lot of Git plugins that will satisfy you. 

![logo-sublime]({% link img/logo-Vim.png %})


Same can be said about [Geany](https://geany.org/) which bear the same characteristics! And several git plugins for Geany exists as well!!! 

![logo-sublime]({% link img/logo-Geany.png %})

You can also use Microsoft Visual Studio (BOOOOOOOOOOOOOH - sorry, I went through terrible nights trying to make a C#/C++ software work). 

The most important is that you must feel confortable using a text editor. Try several of them and do not be afraid to change, most of them have the same qualities!





