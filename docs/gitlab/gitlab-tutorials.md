---
layout: default
title: Tutorials
parent: Git and GitLab
has_children: true
nav_order: 1
---


# Get to grips with GitLab
{: .fs-8 }

Follow this series of tutorials to start working with GitLab
{: .fs-6 .fw-300 }

---

## How to use
Follow the below tutorials (left in the navigation panel) to set up and use GitLab for hosting, communicating, collaborating on and managing your version controlled projects.

**NOTE: These tutorials contain gifs of the processes being demonstrated on GitHub rather than GitLab. These are being included as they are still a useful demonstration. Please stay tuned for a GitLab based update.**

### 1. Tools for git and GitLab
- [1.1 Using the command line]({% link docs/gitlab/1-1-tools-command-line.md %})
- [1.2 Writing in markdown]({% link docs/gitlab/1-2-tools-markdown.md %})
- [1.3 Text editors]({% link docs/gitlab/1-3-tools-atom.md %})

### 2. Getting started
- [2.1 Your GitLab account]({% link docs/gitlab/2-1-starting-gitlab-account.md %})
- [2.2 Global git configuration]({% link docs/gitlab/2-2-starting-configure-git.md %})
- [2.3 Your local repository]({% link docs/gitlab/2-3-starting-local-repo.md %})
- [2.4 Basic git commands]({% link docs/gitlab/2-4-starting-git-basics.md %})

### 3. Collaborating on GitLab
- [3.1 Collaborating on GitLab]({% link docs/gitlab/3-1-collaborating-stranger-or-collaborator.md %})
- [3.2 Collaborating with a stranger]({% link docs/gitlab/3-2-collaborating-fork-their-repo.md %})
- [3.3 Play with this page]({% link docs/gitlab/3-3-collaborating-play-with-this-page.md %})

### 4. Making and publishing your repository
- [4.1 Make a repository]({% link docs/gitlab/4-1-you-make-your-repo.md %})
- [4.2 Create a doi]({% link docs/gitlab/4-2-you-doi.md %})
- [4.3 Create a GitHub Pages site]({% link docs/gitlab/4-3-you-make-your-pages-site.md %})
- [4.4 Create a GitLab Pages site]({% link docs/gitlab/4-4-create-gitlab-pages-site.md %})

### 5. Managing your project on GitLab
- [5.1 GitLab issues]({% link docs/gitlab/5-1-projectmanagement-issues.md %})
- [5.2 GitLab milestones]({% link docs/gitlab/5-2-projectmanagement-milestones.md %})

## Who should I ask for further advice about using GitLab
The Open WIN community are a great resource for support in using GitLab. [Join the community](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/contact/) and try asking your question on slack!

You are very welcome to ask the [Open WIN Engagement Coordinator](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/community/community-who/#community-coordinator---cassandra-gould-van-praag-sheher) for specific advice on using GitLab.

Technical issues on the GitLab instance should be directed to [WIN IT](https://sharepoint.nexus.ox.ac.uk/sites/NDCN/FMRIB/IT/Pages/default.aspx).
