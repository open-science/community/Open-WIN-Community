---
layout: default
title: 2.3 Your local repository
parent: Tutorials
grand_parent: Git and GitLab
has_children: false
nav_order: 6
---


# Create a local version of your repository
{: .fs-8 }

Create a local copy of your first online repository
{: .fs-6 .fw-300 }

---

Currently your "test-git-project" exists online and your computer is configured to access it. We now need to create a local version so you can make changes on your computer and sync them up with the remote (online) version.

In your terminal, move to the directory where you would like your local version of the repository to be stored. Enter the "Create a new repository" commands into your terminal, one line at a time.

Note you will be asked for the SSH key password you generated when you [first accessed your gitlab account]({% link docs/gitlab/2-1-starting-gitlab-account.md %}).

Below is a brief explanation of what each line is doing. You'll learn more about some of these commands in the [git basics tutorial]({% link docs/gitlab/2-4-starting-git-basics.md %}).

After entering these commands, return to the [gitlab webpage](https://git.fmrib.ox.ac.uk) and refresh your project. You will now see that the instructions for creating a new project have gone and the commit message you just entered is shown on the top.

`git clone git@git.fmrib.ox.ac.uk:<your-account-name>/test-git-project.git`
The git clone command creates a copy of the repository (a “clone”) hosted at the address provided, and places it on your local machine in the directory you are currently in. This creates another directory (here called “test-git-project”) which includes all the tracking information required to log the versioning of the contents of this directory.

`cd test-git-project`
This navigates into the newly created directory. Now you are in the new repository folder you can use `ls –la` to view all the files in that directory. You will see that a file `.git` has been created. This relates to the tracking information created in the above step.

`touch README.md`
Touch is a unix command to create an empty file (here called “README.md”). If the file already exists, the touch command updates the files timestamp.

`git add README.md`
The `git add` command adds the file `README.md` to a ‘staging’ area. This is an index of files which will be added or amended in the remote repository. You can check to see what is in the staging area by entering the command `git status`. If you run `git status` after the git add command, you will see that `README.md` is listed as a new file but has not yet been ‘committed’ to the repository.

`git commit -m "add README"`
The `git commit` command records changes to the repository. This stores the current contents of the index (created with add above) in a new commit, with the `–m` flag to instruct the commit to be described with a message. The message is our description of what has been changed in the repository since our last commit. This message should be concise (<72 characters is suggested so it can be read on a single line) and descriptive. More complete descriptions of the commit changes can be added in the form of a full message or log (see this [guide on best practice for commit messages](https://medium.com/@steveamaza/how-to-write-a-proper-git-commit-message-e028865e5791)). The indexed files are still currently in the staging area and not your at your remote repository.

`git push -u origin master`
The `git push` command updates the remote references (those held on the gitlab server) with the local references (those you just created with the git commit command). The `–u` flag adds “upstream” tracking references. `Origin` is a nickname for the remote server and `main` is the name of the “master branch” of your repository, with a branch being the line of development you are working on.
