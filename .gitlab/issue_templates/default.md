## Thank you for contributing to the Open WIN Community
> <p>Please consider reading out guide on [how to submit an issue in GitLab](https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/CONTRIBUTING/).</p>
> <p>Please fill in the details under each of the headings below to give your feedback. You may delete this text when you are done.</p>
> <p> Note: For support requests, please contact open@win.ox.ac.uk. </p>

## I'm submitting a ...
> Put an x in the appropriate square brackets

- [ ] bug/error report (eg. 404)
- [ ] documentation (eg. typos, broken links)
- [ ] feature request (eg. add GitLab tutorial)
- [ ] question (eg. how does this work?)

## Where is the issue?
> Please include links to the affected page(s) referring to specific lines or sentences.

## What is the issue?
> Please describe the problem you have encountered or the feature/content you would like to extend.

## How do we solve the issue?
> Please describe how the problem could be fixed or what additional feature/content could be included.

## Anything else?
> Please give any additional detail which may be relevant or useful, for example detailed explanation, motivation, stacktraces, related issues, links for us to have context.


---
## Fix the issue yourself?
If you are able to, you are welcome to submit new content directly via a merge request. This will ensure you are attributed for your suggestion. See this guide on submitting a merge request via GitLab: https://open.win.ox.ac.uk/pages/open-science/community/Open-WIN-Community/docs/gitlab/3-2-collaborating-fork-their-repo/
